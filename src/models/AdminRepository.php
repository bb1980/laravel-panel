<?php

namespace Progressif\Panel;

use Eloquent;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AdminRepository extends Eloquent implements UserInterface, RemindableInterface {
	
	use UserTrait, RemindableTrait;
	
	protected $table = 'admin';
	
	protected $hidden = array('password', 'remember_token');
	
	
	public function role() {
		return $this->hasOne('\Progressif\Panel\RoleRepository', 'id', 'role_id')->get();
	}
	
}
	