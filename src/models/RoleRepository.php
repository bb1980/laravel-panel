<?php

namespace Progressif\Panel;

use Eloquent;

class AdminRepository extends Eloquent {
	
	protected $table = 'role';
	
	public function admin()
    {
        return $this->hasMany('Progressif\Panel\AdminRepository');
    }
	
}
	