<?php

namespace Progressif\Panel;

use Hash;
use DB;
use Auth;

class AuthPanel extends Auth {

	public function check() {
		return parent::check();
	} 	
	
	public function login($email, $password) {
		$user = AdminRepository::where('email', '=', $email)->get()->first();
		if(!$user)
			return false;
		if($user->password == $password) {
			Auth::loginUsingId($user->id);
	   		return true;
		}
		return false;
	}
	
	public function logout() {
		Auth::logout();
	}
}
