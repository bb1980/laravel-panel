<?php

class AdminTableSeeder extends Seeder {

    public function run()
    {
    	Eloquent::unguard();
		
		
        DB::table('admin')->delete();
		
        DB::table('admin')->insert(array('email' => 'admin@progressif.pl', 'password' => '12345', 'name' => 'Admin', 'role_id' => '1', 'surname' => 'Admin', 'created_at' => time(), 'updated_at' => time()));
		
    }

}
