<?php

class PanelTableSeeder extends Seeder {

    public function run()
    {
    	Eloquent::unguard();
		$this->call('RoleTableSeeder');
        $this->command->info('Role table seeded!');

		$this->call('AdminTableSeeder');
        $this->command->info('Admin table seeded!');
				
    }

}
