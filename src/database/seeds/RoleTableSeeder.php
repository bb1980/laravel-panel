<?php

class RoleTableSeeder extends Seeder {

    public function run()
    {

		DB::table('role')->delete();
		
        DB::table('role')->insert(array('id' => '1', 'name' => 'Administrator', 'description' => 'Administrator', 'created_at' => time()));
		
    }

}
