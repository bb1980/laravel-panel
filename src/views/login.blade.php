@extends('Panel::layouts.login')

@section('content')
 <div class="container">

      <form method="POST" class="form-signin">
        <img class="signin-logo" src="/packages/progressif/panel/img/logo.png" />
        <label for="inputEmail" class="sr-only">Adres e-mail</label>
        <input type="email" id="email" name="email" class="form-control" placeholder="Adres email" required autofocus>
        <label for="inputPassword" class="sr-only">Hasło</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Hasło" required>
        <button class="btn btn-lg btn-primary pull-right" type="submit">Zaloguj</button>
      </form>

    </div>
    
@stop