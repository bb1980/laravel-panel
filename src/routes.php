<?php

Route::any('/panel/login', '\Progressif\Panel\AuthController@login');
Route::any('/panel/logout', '\Progressif\Panel\AuthController@logout');

Route::filter('AuthPanel', function() {
	if(!Auth::check()) {
		return Redirect::to('/panel/login');
	}	
});

Route::group(array('prefix' => 'panel', 'before' => 'AuthPanel'), function() {
	Route::get('/', '\Progressif\Panel\IndexController@index');
});