<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('role', function($table) {
			$table->increments('id');
			$table->string('name');
			$table->string('description');	
			$table->timestamps();
		});
		
		Schema::create('admin', function($table) {
			$table->increments('id');
			$table->string('email');
			$table->string('password');
			$table->string('name');
			$table->string('surname');
			$table->string('remember_token');
			$table->integer('role_id')->unsigned();
			$table->foreign('role_id')->references('id')->on('role');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('admin');
		Schema::dropIfExists('role');
	}

}
