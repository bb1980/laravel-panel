<?php

namespace Progressif\Panel;

use View;

class IndexController extends \Controller {
	
	public function index() {
		return View::make('Panel::index');
	}
}
