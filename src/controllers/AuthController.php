<?php

namespace Progressif\Panel;

use View;
use Input;
use Redirect;
use Progressif\Panel\AuthPanel;


class AuthController extends \Controller {
		
	
	public function login() {
		if((new AuthPanel)->check()) {
			return Redirect::intended('panel');	
		}
		if(Input::server('REQUEST_METHOD') == 'POST') {
			if((new AuthPanel)->login(Input::get('email'), Input::get('password'))) {
				return Redirect::intended('panel');
			}		
		};	
		return View::make('Panel::login');
	}
	
	public function logout() {
		(new AuthPanel)->logout();
		return Redirect::intended('/panel/login');	
	}
	
}